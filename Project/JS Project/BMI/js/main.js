function bmifun() {
    var flag = 1;
    var weight = document.getElementById('weight').value;
    var height = document.getElementById('height').value;
    //  Formula:-- bmi= weight/height*height(in meters);
    height = height * 12; // 12 inch = 1 feet
    height = height * 0.025; // 1 feet = 0.025 meter 

    var bmiResult = weight / (height * height);
    var bmiResult = Math.round(bmiResult);
    document.getElementById('bmivalue').value = bmiResult;


    if (weight == "") {
        document.getElementById('werror').innerHTML = "weight is empty";
        flag = 0;
    } else {
        document.getElementById('werror').innerHTML = "";
        flag = 1;

    }

    if (height == "") {
        document.getElementById('herror').innerHTML = "height is empty";
        flag = 0;
    } else {
        document.getElementById('herror').innerHTML = "";
        flag = 1;

    }

    if (flag) {
        return true;
    } else {
        return false;
    }

}